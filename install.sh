#!/bin/bash
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
file_name="$script_dir/mysql-backup.sh"
chmod +x $file_name
echo '------------'
echo "Installing in $file_name"
echo '------------'
sudo ln -s  "$file_name" /usr/bin/suki-mysql-backup
echo "-Successful-"
echo "Usage: suki-mysql-backup --config=/path/to/your_custom_config.cfg"
echo "------------"