**Mysql-Backup**
==============================

Basic backup for mysql db.

- Remote copy capabilities (PUBLIC KEYS REQUIRED) 
- The configuration file must be defined in some path


**Install**
=====
(sudo required)

- $ chmod +x install.sh
- $ ./install.sh


**Usage**
=====
- Create a config.cfg (use example_config.cfg as template)
- Setup db and other parameters
- in command line 
```
 $ suki-mysql-backup --config=/path/to/your_config.cfg

```


