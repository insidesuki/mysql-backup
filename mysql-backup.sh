#!/bin/bash

show_usage() {
    echo "Usage: $0 --config=/path/to/configfile/config.cfg"
    exit 1
}

# Check if file have at least one argument
if [ "$#" -ne 1 ]; then
    show_usage
fi

# Parse all arguments
for arg in "$@"; do
    case $arg in
        --config=*)
            config_file="${arg#*=}"
            ;;
        *)
            # Unknown argument
            show_usage
            ;;
    esac
done

# check if config file was setted
if [ -z "$config_file" ]; then
    show_usage
fi

# Check if config file exists
if [ ! -e "$config_file" ]; then
    echo "Error: Config file '$config_file' not found."
    exit 1
fi

source "$config_file"

# local files
current_date=`date +%Y%m%d_%H%M%S` 
output_file=$DB_NAME.sql
output_file_date=${output_file}_${current_date}.sql

cd $OUTPUT_PATH
# backup db
mysqldump --opt --host=$DB_HOST --single-transaction --port=$DB_PORT  --password=$DB_PASSWORD --user=$DB_USER --default-character-set=latin1 --triggers --routines  --databases $DB_NAME > $output_file

if [ $? -eq 0 ]
then
  chmod 775 ${output_file}
  tar -zcf ${output_file_date}.tar.gz ${output_file} 
  
    # if remote copy was enable
    if [ "$REMOTE_COPY" = "true" ] || [ "$REMOTE_COPY" = "TRUE" ]; then
        # check  parameters was setted
        if [ -z "$REMOTE_PORT" ] || [ -z "$REMOTE_PATH" ] || [ -z "$REMOTE_HOST" ]; then
                echo "Error: REMOTE_COPY IS TRUE, but REMOTE_HOST, REMOTE_PATH,REMOTE_HOST was not setted!!!."
                exit 1
        fi


        scp -P $REMOTE_PORT ${output_file_date}.tar.gz $REMOTE_HOST:$REMOTE_PATH
    fi
  rm -f ${output_file_date}.tar.gz
 

  echo "Backup successful"
else
  echo "Some Error"
fi

cd $original_path
